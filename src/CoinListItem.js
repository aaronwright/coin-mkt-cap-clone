import React from 'react';

const CoinListItem = (props) => {

    return (
    <div className="single-coin">
        <p> {props.coin.rank}</p> 
        <p> {props.coin.name}</p> 
        <p>{props.coin.symbol}</p> 
        <p>${props.coin.quotes.USD.price}</p>
        <p>${props.coin.quotes.USD.market_cap}</p>
        <button className="btn btn-primary" onClick={() => { props.onSelectCoin(props.coin)}}>SEE MORE</button>
    </div>
    )
}


export default CoinListItem;