import React from 'react';
import CoinListItem from './CoinListItem';


const ListContainer = (props) => {

props.coins.sort((obj1, obj2) => {
    return obj1.rank - obj2.rank;
});


const getCoins = props.coins.map((coin) => {
    return <CoinListItem onSelectCoin={props.onSelectCoin} key={coin.symbol} coin={coin} />;
});
    
    return (
    <div className="list-container">
    <p className="list-header">Top Cryptocurrencies by Market Cap</p>
    <div className="list-table-headers">
        <p>RANK</p>
        <p>NAME</p>
        <p>TICKER</p>
        <p>PRICE ($)</p>
        <p>MARKET CAP</p>        
        <button className="btn btn-warning btn-lg">Learn More</button>                
    </div>
    {getCoins}
    </div>
    );
}


export default ListContainer;