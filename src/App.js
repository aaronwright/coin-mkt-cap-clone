import React, { Component } from 'react';
import axios from 'axios';  
import logo from './logo.svg';
import './App.css';
import ListContainer from './ListContainer';
import CoinModal from './CoinModal';
import Navbar from './Navbar';


class App extends Component {

  constructor() {
    super();

    this.state = {
      coins: [],
    selectedCoin: undefined
    }

    this.onSelectCoin = this.onSelectCoin.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  componentDidMount() {
    console.log('mount!');
    axios.get('https://api.coinmarketcap.com/v2/ticker/?limit=50&sort=rank').then(res => {
      const coinsObj = res.data.data; 


    const coins = Object.values(coinsObj);

      this.setState({coins:coins});
    })
  }


  closeModal = () => {

    this.setState({selectedCoin: undefined});
  }

  onSelectCoin(coin) {
    this.setState({selectedCoin: coin});
  }


  render() {
    return (
      <div className="App">
      <Navbar />
          <div className="container">
              <ListContainer onSelectCoin={this.onSelectCoin} coins={this.state.coins} />
          </div>
          <CoinModal closeModal={this.closeModal} selectedCoin={this.state.selectedCoin} />
      </div>
    );
  }
}

export default App;
