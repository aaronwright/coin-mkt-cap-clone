import React, { Component } from 'react';
import Modal from 'react-modal';



class CoinModal extends Component {


      componentWillMount() {
        Modal.setAppElement('body');
     }


     render() {

      if (this.props.selectedCoin) {
        console.log(this.props.selectedCoin.quotes.USD);
      }

      return (
        <div>
          <Modal
            isOpen={!!this.props.selectedCoin}
            contentLabel={"Selected Coin"}
            onRequestClose={this.props.closeModal}
            closeTimeoutMS={200}
            className="modal"
            overlayClassName="overlayModal"
            >
            <div>
            {this.props.selectedCoin &&
            <div>
              
                 <h3>{this.props.selectedCoin.name}</h3>
               <p>Price: ${this.props.selectedCoin.quotes.USD.price}</p>
               <p>Market Cap: ${this.props.selectedCoin.quotes.USD.market_cap}</p>
               <p>Current Supply: {this.props.selectedCoin.circulating_supply}</p>
               <p>24 HR Volume: {this.props.selectedCoin.quotes.USD.volume_24h}</p>
               <p>Today's Price Change: {this.props.selectedCoin.quotes.USD.percent_change_24h}</p>
               <p>Last 7 Day Price Change: {this.props.selectedCoin.quotes.USD.percent_change_7d}</p>
            </div>
               }
            <button className="btn btn-primary modal-btn" onClick={this.props.closeModal}>CLOSE</button>
            </div>
          </Modal>
        </div>
      )
    }
}


export default CoinModal;
